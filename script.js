//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
/**/
fetch('https://jsonplaceholder.typicode.com/todos',{method: 'GET'}).then((response) => response.json()).then((json) => console.log("#3:",json));


//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
let titles = [];
fetch('https://jsonplaceholder.typicode.com/todos').then(response=>response.json()).then(json=>{json.map((item)=> titles.push(item.title))});
console.log("#4: TITLES",titles)

//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos?id=1',{method: 'GET'}).then(response=> response.json()).then(json=>console.log('#5:',json));

//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1',{method:'GET'}).then(response=> response.json()).then((json) => console.log(`The item "${json.title}" has a status of "${json.completed}"`));



//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'}, 
	body: JSON.stringify({
		userId: 'New',
		id: 1, 
		title: 'CREATE using PUT my TODO list items', 
		completed: false
	})
}).then(response=> response.json()).then(json=>console.log('#7: CREATE',json));


//8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers:{'Content-Type': 'application/json'},
	body: JSON.stringify({
		userId: 1,
		id: 1, 
		title: 'UPDATE using PUT my TODO list items', 
		completed: false
	})
}).then(response=>response.json()).then(json=>console.log('#8 PUT:',json));


//9. Update a to do list item by changing the data structure to contain the following properties:
//- Title
//- Description
//- Status
//- Date Completed
//- User ID
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PATCH',
	headers:{'Content-Type':'application/json'},
	body:JSON.stringify({
		dateCompleted: 'Pending',
		description: 'TO update the my todolist structure',
		id:1,
		status:'Pending',
		title:'New structure To DO list items',
		userId:1
	})
}).then(response=>response.json()).then(json=>console.log('#9:',json));

//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PATCH',
	headers:{'Content-Type':'application/json'},
	body:JSON.stringify({
		dateCompleted: 'Pending',
		description: 'TO update the my todolist with new values',
		id:1,
		status:'Pending',
		title:'Upadtes To DO list items',
		userId:1
	})
}).then(response=>response.json()).then(json=>console.log('#10:',json));

//11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers:{'Content-Type': 'application/json'},
	body: JSON.stringify({
		userId: 1, 
		id: 1,
		title: 'delectus aut autem',
		completed: true,
		status: "Completed",
		date: 'JUNE 1 2022'
	})
}).then(response=>response.json()).then(json=>console.log('#11',json));

//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method:'DELETE'
}).then(console.log('#12: Deleted!'))



